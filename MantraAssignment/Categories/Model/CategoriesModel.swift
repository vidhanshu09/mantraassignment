//
//  CategoriesModel.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 15/03/21.
//

import Foundation
import CoreGraphics

// MARK: - CategoriesModel
struct CategoriesModel: Codable {
    let name: String?
    var height:CGFloat? = 0.0
    var subheight:CGFloat? = 0.0
    let subCategory: [SubCategory]?

    enum CodingKeys: String, CodingKey {
        case name,height
        case subCategory = "sub_category"
    }
}

// MARK: - SubCategory
struct SubCategory: Codable {
    let name, displayName: String?

    enum CodingKeys: String, CodingKey {
        case name
        case displayName = "display_name"
    }
}

