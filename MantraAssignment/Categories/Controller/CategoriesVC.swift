//
//  CategoriesVC.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 15/03/21.
//

import UIKit

class CategoriesVC: UIViewController {
    
    var categoriesViewModel : CategoriesViewModel!
    
    //MARK:- Objects
    
    @IBOutlet weak var categoriesTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        adjustingUI()
        // Do any additional setup after loading the view.
    }
    
    func adjustingUI(){
        categoriesViewModel = CategoriesViewModel(tableView: categoriesTableView)
        categoriesTableView.dataSource = categoriesViewModel
        categoriesTableView.delegate = categoriesViewModel
        categoriesViewModel.getData()
    }

}

class MyOwnTableView: UITableView {
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return self.contentSize
    }

    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }

    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}
