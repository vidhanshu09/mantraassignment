//
//  CategoriesViewModel.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 15/03/21.
//

import Foundation
import UIKit

class CategoriesViewModel:NSObject {
    
    private var tableView: UITableView
    private var categoriesArray = [CategoriesModel]()
    
    init(tableView: UITableView) {
        self.tableView = tableView
    }
    
    func getData(){
        MyLoader.showLoadingView()
        if let url = Bundle.main.url(forResource: "menu", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([CategoriesModel].self, from: data)
                categoriesArray = jsonData
                self.tableView.reloadData()
            } catch {
                print("error:\(error)")
            }
        }
        
        for i in 0..<categoriesArray.count{
            if let subCategory = categoriesArray[i].subCategory{
                for j in 0..<subCategory.count{
                    categoriesArray[i].subheight! += (Utils.heightFor(text: categoriesArray[i].subCategory?[j].name ?? "") + Utils.heightFor(text: categoriesArray[i].subCategory?[j].displayName ?? "")) - 20
                }
            }
        }
        MyLoader.hideLoadingView()
    }
    
    @objc func expand(tapGesture:UITapGestureRecognizer){
        let i : Int = (tapGesture.view?.layer.value(forKey: "expand")) as! Int
        if categoriesArray[i].subCategory?.count != 0{
            if (categoriesArray[i].height ?? 0.0) == 0.0{
                categoriesArray[i].height = 100
            }else{
                categoriesArray[i].height = 0.0
            }
            self.tableView.updateRow(row: i)
        }
    }
    
    
}

extension CategoriesViewModel: UITableViewDataSource,UITableViewDelegate{

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if tableView.tag == 100{
            return categoriesArray.count
        }else{
            return categoriesArray[tableView.tag].subCategory?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if tableView.tag == 100{
            let cell = tableView.dequeueReusableCell(withIdentifier: "category", for: indexPath) as! CategoriesTVC
            cell.shadowView.dropShadow(radius: 5)
            cell.setTableViewDataSourceDelegate(self, forRow: indexPath.row)
            if categoriesArray[indexPath.row].subCategory?.count == 0{
                cell.expandCollapseImageView.isHidden = true
            }else{
                cell.expandCollapseImageView.isHidden = false
            }
            cell.category = categoriesArray[indexPath.row]
            let tapGesture : UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(expand(tapGesture:)))
            cell.sectionView.tag = tableView.tag
            cell.sectionView.layer.setValue(indexPath.row, forKey: "expand")
            cell.sectionView.addGestureRecognizer(tapGesture)
            cell.subcategoryTableView.isScrollEnabled = false
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "subcategory", for: indexPath) as! CategoriesTVC
            cell.subCategory = categoriesArray[tableView.tag].subCategory?[indexPath.row]
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
