//
//  CategoriesTVC.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 15/03/21.
//

import UIKit

class CategoriesTVC: UITableViewCell {
    
    @IBOutlet weak var sectionView: UIView!
    @IBOutlet weak var expandCollapseImageView: UIImageView!
    @IBOutlet weak var sectionNameLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var subcategoryTableView: MyOwnTableView!
    @IBOutlet weak var subcategoryTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var subcategorynameLabel: UILabel!
    @IBOutlet weak var subcategorydescLabel: UILabel!
    
    var category:CategoriesModel?{
        didSet{
            sectionNameLabel.text = category?.name
            if (category?.height ?? 0) != 0.0{
                subcategoryTableViewHeightConstraint.constant = category?.subheight ?? 0.0
                expandCollapseImageView.image = #imageLiteral(resourceName: "collapseImage")
            }else{
                subcategoryTableViewHeightConstraint.constant = 0.0
                expandCollapseImageView.image = #imageLiteral(resourceName: "expandImage")
            }
            
        }
    }
    
    var subCategory:SubCategory?{
        didSet{
            subcategorynameLabel.text = subCategory?.name
            subcategorydescLabel.text = subCategory?.displayName
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CategoriesTVC {
    
    func setTableViewDataSourceDelegate<D: UITableViewDataSource & UITableViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        subcategoryTableView.tag = row
        subcategoryTableView.delegate = dataSourceDelegate
        subcategoryTableView.dataSource = dataSourceDelegate
        subcategoryTableView.estimatedRowHeight = 30.0
        subcategoryTableView.rowHeight = UITableView.automaticDimension
        subcategoryTableView.reloadData()
    }
}

