//
//  Utils.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 16/03/21.
//

import Foundation
import CoreGraphics
import UIKit

class Utils {
    
    static let labelOffset: CGFloat = 10
    
    static func heightFor(text: String) -> CGFloat {
        text.height(width: UIScreen.main.bounds.width - 2 * self.labelOffset,
                    font: .systemFont(ofSize: 17, weight: .regular)) + 2 * self.labelOffset
    }
    
}
