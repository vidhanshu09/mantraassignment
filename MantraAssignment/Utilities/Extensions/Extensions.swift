//
//  Extensions.swift
//  MantraAssignment
//
//  Created by Vidhanshu Bhardwaj on 15/03/21.
//

import Foundation
import UIKit

extension UIView{
    func dropShadow(radius:CGFloat? = 10) {
        layer.cornerRadius = radius ?? 10
        layer.masksToBounds = true
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.5
        layer.masksToBounds = false
    }
}

extension UITableView
{
    func updateRow(row: Int, section: Int = 0)
    {
        let indexPath = IndexPath(row: row, section: section)
        
        self.beginUpdates()
        self.reloadRows(at: [indexPath as IndexPath], with: .automatic)
        self.endUpdates()
    }
    
}

extension String {
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let rect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
}
